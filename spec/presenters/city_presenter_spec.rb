# frozen_string_literal: true

require 'rails_helper'

RSpec.describe CityPresenter do
  subject(:presenter) do
    described_class.new(city)
  end

  let(:api_response) do
    JSON.parse(File.read('spec/fixtures/delhi-weather.json'))
  end

  let(:city) { City.new(api_response) }

  describe '#name_with_country' do
    subject { presenter.name_with_country }

    it { is_expected.to eq('Delhi, IN') }
  end

  describe '#weather_icon_src' do
    subject { presenter.weather_icon_src }

    it { is_expected.to eq('http://openweathermap.org/img/w/50d.png') }
  end

  describe '#formatted_sunrise_time' do
    subject { presenter.formatted_sunrise_time }

    it { is_expected.to eq(' 6:54 am') }
  end

  describe '#formatted_sunset_time' do
    subject { presenter.formatted_sunset_time }

    it { is_expected.to eq(' 6:15 pm') }
  end

  describe '#formatted_temperature' do
    subject { presenter.formatted_temperature(40) }

    it { is_expected.to eq('40 °C') }
  end

  describe '#formatted_pressure' do
    subject { presenter.formatted_pressure(1020) }

    it { is_expected.to eq('1020 hPa') }
  end

  describe '#formatted_humidity' do
    subject { presenter.formatted_humidity(45) }

    it { is_expected.to eq('45 %') }
  end

  describe '#formatted_wind_direction' do
    subject { presenter.formatted_wind_direction }

    it { is_expected.to eq('WSW') }
  end

  describe '#formatted_visibility' do
    subject { presenter.formatted_visibility }

    it { is_expected.to eq('3 kms') }
  end
end
