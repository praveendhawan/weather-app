# frozen_string_literal: true

require 'rails_helper'

RSpec.describe City::Coordinates do
  subject(:coordinates) { described_class.new(coordinates_details) }

  let(:api_response) do
    JSON.parse(File.read('spec/fixtures/delhi-weather.json'))
  end

  let(:coordinates_details) { api_response['coord'] }
  let(:delhi_lat) { 28.6667 }
  let(:delhi_lon) { 77.2167 }

  describe '#latitude' do
    subject { coordinates.latitude }

    it { is_expected.to eq(delhi_lat) }
  end

  describe '#longitude' do
    subject { coordinates.longitude }

    it { is_expected.to eq(delhi_lon) }
  end
end
