# frozen_string_literal: true

require 'rails_helper'

RSpec.describe City::Weather do
  subject(:weather) do
    described_class.new(
      weather_details,
      wind_details,
      snow_details,
      rain_details,
      cloud_details
    )
  end

  let!(:api_response) do
    JSON.parse(File.read('spec/fixtures/delhi-weather.json'))
  end

  let(:weather_details) { api_response['weather'] }
  let(:wind_details) { api_response['wind'] }
  let(:snow_details) { api_response['snow'] }
  let(:rain_details) { api_response['rain'] }
  let(:cloud_details) { api_response['clouds'] }

  describe '#humanized_weather_conditions' do
    subject { weather.humanized_weather_conditions }

    it { is_expected.to eq([weather_details.first['main']]) }
  end

  describe '#primary_humanized_weather' do
    subject { weather.primary_humanized_weather }

    it { is_expected.to eq(weather_details.first['main']) }
  end

  describe '#weather_icons' do
    subject { weather.weather_icons }

    it { is_expected.to eq([weather_details.first['icon']]) }
  end

  describe '#wind_speed' do
    subject { weather.wind_speed }

    it { is_expected.to eq(wind_details['speed']) }
  end

  describe '#wind_direction' do
    subject { weather.wind_direction }

    it { is_expected.to eq(wind_details['deg']) }
  end

  describe '#wind_gust' do
    subject { weather.wind_gust }

    it { is_expected.to eq(wind_details['gust']) }
  end

  describe '#cloudiness' do
    subject { weather.cloudiness }

    it { is_expected.to eq(cloud_details['all']) }
  end

  describe '#last_1_hour_rain' do
    subject { weather.last_1_hour_rain }

    it { is_expected.to eq(rain_details&.fetch('1h')) }
  end

  describe '#last_3_hour_rain' do
    subject { weather.last_3_hour_rain }

    it { is_expected.to eq(rain_details&.fetch('3h')) }
  end

  describe '#last_1_hour_snow' do
    subject { weather.last_1_hour_snow }

    it { is_expected.to eq(snow_details&.fetch('1h')) }
  end

  describe '#last_3_hour_snow' do
    subject { weather.last_3_hour_snow }

    it { is_expected.to eq(snow_details&.fetch('3h')) }
  end
end
