# frozen_string_literal: true

require 'rails_helper'

RSpec.describe City::Temperature do
  let(:api_response) do
    JSON.parse(File.read('spec/fixtures/delhi-weather.json'))
  end

  let(:temperature_details) { api_response['main'] }

  subject(:temperature) { described_class.new(temperature_details) }

  describe '#minimum' do
    subject { temperature.minimum }

    it { is_expected.to eq(temperature_details['temp_min']) }
  end

  describe '#maximum' do
    subject { temperature.maximum }

    it { is_expected.to eq(temperature_details['temp_max']) }
  end

  describe '#current' do
    subject { temperature.current }

    it { is_expected.to eq(temperature_details['temp']) }
  end

  describe '#feels_like' do
    subject { temperature.feels_like }

    it { is_expected.to eq(temperature_details['feels_like']) }
  end

  describe '#humidity' do
    subject { temperature.humidity }

    it { is_expected.to eq(temperature_details['humidity']) }
  end

  describe '#pressure' do
    subject { temperature.pressure }

    it { is_expected.to eq(temperature_details['pressure']) }
  end

  describe '#sea_level_pressure' do
    subject { temperature.sea_level_pressure }

    it { is_expected.to eq(temperature_details['sea_level']) }
  end

  describe '#ground_level_pressure' do
    subject { temperature.ground_level_pressure }

    it { is_expected.to eq(temperature_details['grnd_level']) }
  end
end
