# frozen_string_literal: true

require 'rails_helper'

RSpec.describe City do
  subject(:city) do
    described_class.new(api_response)
  end

  let(:api_response) do
    JSON.parse(File.read('spec/fixtures/delhi-weather.json'))
  end

  describe 'delegation of methods' do
    it { is_expected.to delegate_method(:latitude).to(:coordinates) }
    it { is_expected.to delegate_method(:longitude).to(:coordinates) }
    it { is_expected.to delegate_method(:humanized_weather_conditions).to(:weather) }
    it { is_expected.to delegate_method(:primary_humanized_weather).to(:weather) }
    it { is_expected.to delegate_method(:weather_icons).to(:weather) }
    it { is_expected.to delegate_method(:primary_weather_icon).to(:weather) }
    it { is_expected.to delegate_method(:wind_speed).to(:weather) }
    it { is_expected.to delegate_method(:wind_direction).to(:weather) }
    it { is_expected.to delegate_method(:wind_gust).to(:weather) }
    it { is_expected.to delegate_method(:cloudiness).to(:weather) }
    it { is_expected.to delegate_method(:last_1_hour_rain).to(:weather) }
    it { is_expected.to delegate_method(:last_3_hour_rain).to(:weather) }
    it { is_expected.to delegate_method(:last_1_hour_snow).to(:weather) }
    it { is_expected.to delegate_method(:last_3_hour_snow).to(:weather) }
    it { is_expected.to delegate_method(:minimum).to(:temperature).with_prefix }
    it { is_expected.to delegate_method(:maximum).to(:temperature).with_prefix }
    it { is_expected.to delegate_method(:current).to(:temperature).with_prefix }
    it { is_expected.to delegate_method(:feels_like).to(:temperature) }
    it { is_expected.to delegate_method(:humidity).to(:temperature) }
    it { is_expected.to delegate_method(:pressure).to(:temperature) }
    it { is_expected.to delegate_method(:sea_level_pressure).to(:temperature) }
    it { is_expected.to delegate_method(:ground_level_pressure).to(:temperature) }
  end

  describe '#name' do
    subject { city.name }

    it { is_expected.to eq(api_response['name']) }
  end

  describe '#country_code' do
    subject { city.country_code }

    it { is_expected.to eq(api_response.dig('sys', 'country')) }
  end

  describe '#sunrise_time' do
    subject { city.sunrise_time }

    it { is_expected.to be_a_kind_of ActiveSupport::TimeWithZone }
  end

  describe '#sunset_time' do
    subject { city.sunset_time }

    it { is_expected.to be_a_kind_of ActiveSupport::TimeWithZone }
  end

  describe '#visibility' do
    subject { city.visibility }

    it { is_expected.to eq(api_response['visibility']) }
  end

  describe '#timezone_shift' do
    subject { city.timezone_shift }

    it { is_expected.to eq(api_response['timezone']) }
  end

  describe '#not_found?' do
    subject { city.not_found? }

    it { is_expected.to be_falsy }

    context 'when city not found response' do
      let(:api_response) do
        JSON.parse(File.read('spec/fixtures/city-not-found.json'))
      end

      it { is_expected.to be_truthy }
    end
  end

  describe '#error_message' do
    subject { city.error_message }

    it { is_expected.to eq(nil) }

    context 'when city not found response' do
      let(:api_response) do
        JSON.parse(File.read('spec/fixtures/city-not-found.json'))
      end

      it { is_expected.to eq(api_response['message']) }
    end
  end
end
