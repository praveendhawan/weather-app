# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Openweathermap::ApiClient do
  describe '.call' do
    subject(:client) { described_class.client }

    it { is_expected.to be_a Faraday::Connection }

    it 'has correct appId associated' do
      expect(client.params.keys).to include('appId')
      expect(client.params['appId']).
        to(eq(Rails.application.credentials.config[:openweathermap][:api_key]))
    end
  end
end
