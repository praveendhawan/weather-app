# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Openweathermap::ApiWrapper do
  subject(:api_wrapper) { described_class.new }

  let(:city_name) { 'delhi' }

  vcr_options = { cassette_name: 'openweathermap/current_weather' }

  describe '#current_weather_for', vcr: vcr_options do
    subject(:weather) { api_wrapper.current_weather_for(city_name) }

    context 'when city is found' do
      it 'gives correct result when city is found' do
        expect(weather).to be_a Hash

        expect(weather.keys).to include('coord', 'weather', 'main')
      end
    end

    context 'when city is not found' do
      let(:city_name) { 'my-random-city-name-which-does-not-exists' }

      it 'gives 404' do
        expect(weather).to be_a Hash

        expect(weather.keys).to include('cod', 'message')
      end
    end
  end
end
