# frozen_string_literal: true

# Presenter for city
class CityPresenter
  TIME_FORMAT = '%l:%M %P'
  CIRCLE_DEGREES = 360
  SECTOR_DEGRESS = 22.5
  METERS_IN_KMS = 1000
  DIRECTIONS = %w[
    N
    NNE
    NE
    ENE
    E
    ESE
    SE
    SSE
    S
    SSW
    SW
    WSW
    W
    WNW
    NW
    NNW
  ].freeze

  def initialize(city)
    @city = city
  end

  def name_with_country
    "#{city.name}, #{city.country_code}"
  end

  def weather_icon_src
    "http://openweathermap.org/img/w/#{city.primary_weather_icon}.png"
  end

  def formatted_sunrise_time
    city.sunrise_time.strftime(TIME_FORMAT)
  end

  def formatted_sunset_time
    city.sunset_time.strftime(TIME_FORMAT)
  end

  def formatted_temperature(temperature)
    "#{temperature} °C"
  end

  def formatted_pressure(pressure)
    "#{pressure} hPa"
  end

  def formatted_humidity(humidity)
    "#{humidity} %"
  end

  def formatted_wind_direction
    normalized_direction = city.wind_direction % CIRCLE_DEGREES

    DIRECTIONS[(normalized_direction / SECTOR_DEGRESS).floor]
  end

  def formatted_visibility
    if city.visibility > (METERS_IN_KMS / 2)
      "#{city.visibility / METERS_IN_KMS} kms"
    else
      "#{city.visibility} m"
    end
  end

  private

  attr_reader :city
end
