# frozen_string_literal: true

# HomeControllerEntryPoint for our App
class HomeController < ApplicationController
  def index; end

  def search
    redirect_to root_path unless params[:query].present?

    details = Openweathermap::ApiWrapper.new.current_weather_for(params[:query])
    @city = City.new(details)
  end
end
