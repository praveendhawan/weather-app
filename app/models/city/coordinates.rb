# frozen_string_literal: true

class City
  # Coordinates details for the city
  class Coordinates
    def initialize(coordinates_details)
      @coordinates_details = coordinates_details
    end

    def latitude
      coordinates_details['lat']
    end

    def longitude
      coordinates_details['lon']
    end

    private

    attr_reader :coordinates_details
  end
end
