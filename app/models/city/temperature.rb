# frozen_string_literal: true

class City
  # Temperature details for the city
  class Temperature
    # @params temperature_details [Hash]
    def initialize(temperature_details)
      @temperature_details = temperature_details
    end

    def minimum
      temperature_details['temp_min']
    end

    def maximum
      temperature_details['temp_max']
    end

    def current
      temperature_details['temp']
    end

    def feels_like
      temperature_details['feels_like']
    end

    def humidity
      temperature_details['humidity']
    end

    def pressure
      temperature_details['pressure']
    end

    def sea_level_pressure
      temperature_details['sea_level']
    end

    def ground_level_pressure
      temperature_details['grnd_level']
    end

    private

    attr_reader :temperature_details
  end
end
