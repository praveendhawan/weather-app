# frozen_string_literal: true

class City
  # General weather details for the city
  class Weather
    # @params weather [Hash]
    # @params wind [Hash]
    # @params snow [Hash]
    # @params rain [Hash]
    # @params cloud [Hash]
    def initialize(weather, wind, snow, rain, cloud)
      @weather = weather
      @wind = wind
      @snow = snow
      @rain = rain
      @cloud = cloud
    end

    def humanized_weather_conditions
      weather.map { |details| details['main'] }
    end

    def primary_humanized_weather
      humanized_weather_conditions.first
    end

    def weather_icons
      weather.map { |details| details['icon'] }
    end

    def primary_weather_icon
      weather_icons.first
    end

    def wind_speed
      wind['speed']
    end

    def wind_direction
      wind['deg']
    end

    def wind_gust
      wind['gust']
    end

    def cloudiness
      cloud['all']
    end

    def last_1_hour_rain
      rain&.fetch('1h')
    end

    def last_3_hour_rain
      rain&.fetch('3h')
    end

    def last_1_hour_snow
      snow&.fetch('1h')
    end

    def last_3_hour_snow
      snow&.fetch('3h')
    end

    private

    attr_reader :weather, :wind, :rain, :snow, :cloud
  end
end
