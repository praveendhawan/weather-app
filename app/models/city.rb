# frozen_string_literal: true

# Wrappepr to make Ruby objects for Openweathermap weather details
class City
  NOT_FOUND = '404'

  # @params details [Hash]
  def initialize(details)
    @details = details

    @coordinates = Coordinates.new(details['coord'])
    @weather = Weather.new(
      details['weather'],
      details['wind'],
      details['snow'],
      details['rain'],
      details['cloud']
    )
    @temperature = Temperature.new(details['main'])
  end

  delegate :latitude, :longitude, to: :coordinates

  delegate :humanized_weather_conditions,
           :primary_humanized_weather,
           :weather_icons,
           :primary_weather_icon,
           :wind_speed,
           :wind_direction,
           :wind_gust,
           :cloudiness,
           :last_1_hour_rain,
           :last_3_hour_rain,
           :last_1_hour_snow,
           :last_3_hour_snow,
           to: :weather

  delegate :minimum,
           :maximum,
           :current,
           to: :temperature, prefix: true

  delegate :feels_like,
           :humidity,
           :pressure,
           :sea_level_pressure,
           :ground_level_pressure,
           to: :temperature

  def name
    details['name']
  end

  def country_code
    details.dig('sys', 'country')
  end

  def sunrise_time
    Time.zone.at(details.dig('sys', 'sunrise') + timezone_shift)
  end

  def sunset_time
    Time.zone.at(details.dig('sys', 'sunset') + timezone_shift)
  end

  def visibility
    details['visibility']
  end

  def timezone_shift
    details['timezone']
  end

  def not_found?
    details['cod'] == NOT_FOUND
  end

  def error_message
    details['message']
  end

  private

  attr_reader :details, :weather, :temperature, :coordinates
end
