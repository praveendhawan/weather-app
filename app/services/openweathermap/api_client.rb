# frozen_string_literal: true

module Openweathermap
  # Openweathermap API client
  class ApiClient
    BASE_URL = 'http://api.openweathermap.org/data/2.5'
    API_KEY = Rails.application.credentials.config[:openweathermap][:api_key]
    MEASUREMENT_UNIT = :metric

    def self.client
      @client ||= Faraday.new(
        url: BASE_URL,
        params: {
          appId: API_KEY,
          units: MEASUREMENT_UNIT
        },
        headers: {
          'Content-Type' => 'application/json'
        }
      )
    end
  end
end
