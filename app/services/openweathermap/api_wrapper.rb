# frozen_string_literal: true

module Openweathermap
  # Openweathermap API wrapper to make requests
  class ApiWrapper
    # @params city_name [String]
    # @return [Hash]
    def current_weather_for(city_name)
      response = ApiClient.client.get('weather') do |request|
        request.params['q'] = city_name
      end

      JSON.parse(response.body)
    end
  end
end
