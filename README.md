# README

## Weather App for Genome

Requirements

* Versions
-> Ruby - 2.6.1
-> Postgres - 13
-> Yarn - 1.22.10
-> Node - v15.8.0
*-> Rails - 6.0.0.2

* Configuration

```
bundle install
yarn install
```

* Database creation
```
rails db:create
```

* Database initialization
```
rails db:migrate

```

* How to run the test suite

```
rspec .
```

* Start Rails server

```
rails s
```
Now open up `localhost:3000` in your browser to check if server is running

### Screenshots

Home Page
![Home Page](screenshots/weather-home.png)

Delhi Weather
![Delhi](screenshots/delhi-weather.png)

Moscow Weather
![Moscow](screenshots/moscow-weather.png)

City not Found
![Not Found](screenshots/city-not-found.png)

Improvements

1. Add caching
2. Add another controller for Search
3. Add historical and forecasts of weather for the city
4. Feature Specs
5. Add more beautiful UI
